package com.anjali.app;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.EventBusOptions;
import io.vertx.core.http.ClientAuth;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.PemKeyCertOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.handler.AuthHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.ext.web.handler.sockjs.SockJSHandlerOptions;
import io.vertx.ext.web.sstore.LocalSessionStore;
import java.util.concurrent.TimeUnit;
import io.vertx.core.json.JsonObject;

/**
 * Hello world!
 *
 */
public class App{

    public static void main( String[] args ) {
        //Before you can use Vertx, you have to create an instance of vertx
        //When you instantiation vertx, you set options for the EventBus
        //The options I have here are for security
        //For more information see: http://vertx.io/docs/apidocs/io/vertx/core/eventbus/EventBusOptions.html
        Vertx vertx = Vertx.vertx(new VertxOptions()
        .setEventBusOptions(new EventBusOptions()
            .setSsl(true)
            .setClientAuth(ClientAuth.REQUIRED)
            .setTcpKeepAlive(true)
            .setTrustAll(false)
        ));

        //Setting up a router object so that routes to different topics can be made
        Router router = Router.router(vertx);

        //Only send messages with the specified topic
        //For more information see: http://vertx.io/docs/vertx-web/java/#_sockjs_event_bus_bridge
        BridgeOptions options = new BridgeOptions().addOutboundPermitted(new PermittedOptions().setAddress("some-topic"));
        //Creating a handler for the topic
        SockJSHandler sockJSHandler = SockJSHandler.create(vertx).bridge(options);
        //Create a route for that handler
        router.route("/Testing/*").handler(sockJSHandler);

        // Start the web server and tell it to use the router to handle requests.
        vertx.createHttpServer().requestHandler(router::accept).listen(8080);

        //Setting up the eventBus object
        EventBus eb = vertx.eventBus();

        while (true) {
            try{
                TimeUnit.SECONDS.sleep(5);
            } catch (Exception e) {}

            System.out.println("Publishing message");

            eb.publish("some-topic", new JsonObject().put("greeting", "Hello"));
        }

    }
}
