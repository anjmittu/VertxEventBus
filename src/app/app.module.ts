import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SocketConnection } from './SocketConnection';
import { VertxService } from './app.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [VertxService, SocketConnection],
  bootstrap: [AppComponent]
})
export class AppModule { }
