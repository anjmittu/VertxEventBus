import { Injectable } from '@angular/core';
import * as EventBus from 'vertx3-eventbus-client';


@Injectable()
export class SocketConnection {

    public addMessage = (message) => {
        console.log("Have a message");
        console.log("I have received a message: " + message.body());
    }

    public connectToSocket() {
        var messages = [];
        var observerCallbacks = {};

        // initialize
        console.log("in the function");
        var eb = new EventBus('http://localhost:8080/Testing');
        eb.onopen = function() {
            console.log("opened socket");
            // set a handler to receive a message
            eb.registerHandler('some-topic', function(error, message) {
                console.log("Have a message");
                console.log("I have received a message: " + JSON.stringify(message));
            });

        }
    }
}