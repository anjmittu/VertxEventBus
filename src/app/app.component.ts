import { Component } from '@angular/core';
import { VertxService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [VertxService]
})
export class AppComponent {

  constructor(private vertxService: VertxService) {}

  title = 'Vertx Test';
}
