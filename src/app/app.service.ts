import { Injectable } from '@angular/core';
import { SocketConnection } from './SocketConnection';

@Injectable()
export class VertxService {

    constructor(private socketConnection: SocketConnection) {
        socketConnection.connectToSocket();
    }

}