import { VertxEventBusPage } from './app.po';

describe('vertx-event-bus App', () => {
  let page: VertxEventBusPage;

  beforeEach(() => {
    page = new VertxEventBusPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
